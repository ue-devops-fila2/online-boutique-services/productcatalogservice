# Déploiement du catalogue de produits
## Récupération des sources
Cloner ce repository

### Installation de Go (https://go.dev/doc/install)
Le catalogue de produit est écrit en Go. Il faut donc dans un premier temps installer Go: https://go.dev/dl. 
Dans notre cas, nous souhaitons installer la version **1.19.3**, pour les machines **linux-amd64**.

[//]: # (Les binaires de Go doivent être accessibles depuis le PATH pour pouvoir être utilisés dans la suite.)
[//]: # (Un dossier usuellement présent dans le PATH est le dossier `/usr/local/bin`. Vous pouvez donc créer un **lien symbolique**)
[//]: # (dans `/usr/local/bin` pointant vers les **binaires** de Go préalablement installés: `/path/to/go/bin` de façon à les rendre accessible de manière globale.)

Vérifier que l'installation de go est accessible: `go version`.

### Installation et build du microservice
Le catalogue de produit communique via gRPC. Il faut donc compiler les fichiers `.proto` permettant d'obtenir les services correspondant en Go.\
Pour cela, il faut installer les paquets `apt` suivants: `protobuf-compiler` et `golang-goprotobuf-dev`. 
Ces paquets contiennent l'outil `protoc` permettant de compiler les fichiers .proto dans un langage donné.

Installer les dépendances du service en vous positionnant dans le dossier contenant le microservice et en utilisant la commande `go mod download`.\
Ces modules continennent notamment le plugin permettant de compiler les fichiers .proto en Go.

Créer le dossier `genproto`, puis compiler le fichier `productcatalogservice.proto` via la commande suivantes:\
`protoc --go_out=plugins=grpc:genproto -I . productcatalogservice.proto`

Builder le projet via la commande `go build`. Cette commande peut prendre en argument le fichier de destination (`-o`)

### Lancement du service
**Executer le binaire** résultant du go build. Préciser la variable d'environnement **DISABLE_PROFILER=1** pour désactiver le profiler qui n'est pas utile pour ce TP.
